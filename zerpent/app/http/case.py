from flask import Blueprint

from zerpent.app.asl import dispatch
from zerpent.domain.case.commands import TransitionCaseCommand
from zerpent.domain.case.service import Service
from zerpent.util import build_v1_response

case = Blueprint("case", __name__)


@case.route("/<uuid:case_id>/transition")
def transition(case_id):
    command = TransitionCaseCommand(case_id)
    reference_id = dispatch(command, Service())

    return build_v1_response(
        {"type": "command", "reference": reference_id, "instance": None}
    )
