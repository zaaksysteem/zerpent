from zerpent.util import build_v1_response


def endpoint_not_found(e):
    """404 - Not found error handler

    Returns an `exception` result

    """
    return build_v1_response(
        {
            "type": "exception",
            "reference": None,
            "instance": {
                "type": "api/v1/endpoint_not_found",
                "message": "This endpoint does not exist",
            },
        },
        status=404,
    )


def internal_server_error(e):
    """500 - Internal server error and generic exception handler

    Returns an `exception` result

    """

    return build_v1_response(
        {
            "type": "exception",
            "reference": None,
            "instance": {
                "type": "api/v1/internal_server_error",
                "message": str(e),
            },
        },
        status=500,
    )
