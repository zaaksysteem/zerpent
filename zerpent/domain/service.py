from typing import List

from zerpent.domain.command import Command
from zerpent.domain.event import Event


class Service:
    """Abstract base class for domain services"""

    def execute(self, command: Command, **kwargs) -> List[Event]:
        raise NotImplementedError
