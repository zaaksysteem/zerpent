from typing import NamedTuple


class Command(NamedTuple):
    domain: str
