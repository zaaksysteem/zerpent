# Contributing to the Zerpent project

Thank you for your support and effort in helping this project grow. We welcome
all contributors to our codebase. And by reading the following guidelines, we
are hoping you get al the information you need to write beautiful and working
code.

## TL;DR

```
git config core.hooksPath bin/git-hooks
git config commit.template .git-commit-template
```


## Code structure

This project uses a lot of DDD (Domain Driven Design) patterns, which is a
great method for writing complex applications. It gives us a method to write
sustainable code, and prevents us from the pitfall of tighly coupling too
much concepts of our application.

**Repository structure**

```
  zerpent                   Source code of the main application

  zerpent.app               [application] Application layer, here is where the
                            http daemon or messagebus listener would reside.
  zerpent.app.service       Application services
  zerpent.app.api           API helpders
  zerpent.app.http          HTTP daemon, e.g. Flask or Pyramid
  zerpent.app.http.api      GLUE: HTTP glue to API
  zerpent.app.http.views    API Views
  zerpent.app.http.views.bastaard    Example view

  zerpent.domain            [domain] Domain layer, here is where all the
                            bussiness logic resides
  zerpent.domain.bastaard   Example domain
  zerpent.infrastructure    Infrastructure layer
```

## Code location

This code is hosted on gitlab, please see the README.md for the exact
location of this specific codebase. We use `git` as our repository software.

## Code quality

Along with readable code, we like extensible documentation, unit tests and
a solid understanding of the location of our code. We use the following tools
to make sure we do not forget something (PEP8):

    isort   # Makes sure all those "import" lines are sorted
    black   # Makes sure we use the proper indentation, line width, etc.
    flake8  # Code quality and linting

In short:

* Use spaces instead of tabs
* Make sure your maximum line width does not exceed 79 characters
* Write a test for every function
* Write your documentation

Or, just make it easey for yourself, and use the proper tools for the job:

### Git setup

We provided some git-hooks, which can check your code for proper indentation
and style.

On the commandline, run:

```
git config core.hooksPath bin/git-hooks
```

### Editor setup

* Plase use the editoconfiguration from .editorconfig, instructions:
  https://editorconfig.org/
* For VIM: https://realpython.com/vim-and-python-a-match-made-in-heaven/

### Sublime

**Package Control: Install packages**

**sublack**

```
{
  "black_line_length": 79,
  "black_on_save": true,
 }
```

**Python Flake8 Lint** 
```
{

  "ignore": ["E203", "E266", "E501", "W503"],
  "complexity": 10,
}
```


## Committing code

After you followed our code quality guidelines above, you are ready for a code
review from someone else. We assume you already forked the project to your
own private repository.

### Commit message

Please rebase your changes to a single commit describing your change. Make
sure we know whether it is a feature or a bugfix, or simply, follow the given
commit message template `.git-commit-template`. You can set it up as a default
by running:

```
git config commit.template .git-commit-template
```

### Merge request

Now that you have a solid commmit message describing the change you've made
to the software, it is time to create a merge request, so other people can
review your changes. Use the "Merge request" button from within gitlab to do
just that.

People will think something about your implementation, but they will allways
make sure you know that to do with it. The rules below describe the checks
a developer does when reviewing your code.

#### Code review rules

* Every function contains valid documentation
* Every function has a sunny day unit test

#### Code review template

When giving feedback to fellow developers, using more words is always better
than none. In addition to it, we like to make sure you know what to do with
the information by using the following emoji's ;)

* :bulb: 💡 Code is okay, but by using this suggestion, it will be even
better
* :+1: 👍 Code is ok, just some feedback!
* :-1: 👎Code is not ok, it does not pass the code review rules above

Please make sure you do not only use the emoji's above, but also tell the
developer what is wrong and what he could do to fix it.

**Examples:**

* :bulb: I like your solution, but instead of ` a = a + 1 ` you could also
increment the variable without extra code by doing: ` a += 1 `.
* :+1: Nice work! I really like what you did by refactoring this piece of code
* :-1: Missing documentation: at function `addProject` you forgot to mention
the required parameter `project`

Please make sure you approve the merge request at the top when you did not
find any showstoppers (thumbs down), this way the other party can decide what
to do next: leave it this way, or improve and process the given suggestions
and showstoppers.

